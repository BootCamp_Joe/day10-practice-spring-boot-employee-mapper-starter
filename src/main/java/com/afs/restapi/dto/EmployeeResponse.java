package com.afs.restapi.dto;

public class EmployeeResponse {
    private Long id;
    private String name;
    private Integer age;
    private String gender;
    private Long companyId;

    public EmployeeResponse(Long id, String name, Integer age, String gender, Long companyId) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.companyId = companyId;
    }

    public EmployeeResponse() {
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getAge() {
        return age;
    }

    public String getGender() {
        return gender;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }
}
