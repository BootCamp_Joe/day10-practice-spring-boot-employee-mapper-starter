package com.afs.restapi.dto;

public class EmployeeRequest {
    private Integer age;
    private String name;
    private String gender;
    private Integer salary;
    private Long companyId;

    public EmployeeRequest(Integer age, String name, String gender, Integer salary, Long companyId) {
        this.age = age;
        this.name = name;
        this.gender = gender;
        this.salary = salary;
        this.companyId = companyId;
    }

    public EmployeeRequest() {
    }

    public Integer getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    public String getGender() {
        return gender;
    }

    public Integer getSalary() {
        return salary;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }
}
